Gem::Specification.new do |s|
  s.name         = 'tmus'
  s.version      = '0.2.0'
  s.licenses     = ['MIT']
  s.summary      = 'Team Me Up, Scotty! - One for All and All for One'
  s.description  = 'tmus helps you get and stay in sync with your team on all shared files'
  s.requirements << 'GPGME available on the PATH'
  s.authors      = ['Eric Platon', 'Kaz Takahashi']
  s.email        = 'zaraki@gmx.com, takahashi.kazuyoshi@gmail.com'
  s.files        = Dir.glob('**/*.rb')
  s.bindir       = 'bin'
  s.executables  << 'tmus'
  s.homepage     = 'https://bitbucket.org/chikaku/tmus'

  s.add_runtime_dependency 'gpgme', '~> 2.0', '>= 2.0.12'
  s.add_dependency 'thor',  '~>0.19.1'
end


require File.expand_path(File.join('..', '..', 'lib', 'app'), __FILE__)

require 'byebug'

# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration
RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    # Will default to `true` in RSpec 4.
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    # Will default to `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end

  config.example_status_persistence_file_path = "spec/rspec_run_cache"

  config.disable_monkey_patching!

  if config.files_to_run.one?
    config.default_formatter = 'doc'
  end

  config.profile_examples = 3

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random

  Kernel.srand config.seed
end

#
# Wrap GPGMe for testing
#
require 'gpgme'
require 'tmpdir'

module Tmus
  class Test
    class << self
      require File.expand_path(File.join('data', 'keys'), File.dirname(__FILE__))

      def tmus_test_config
        {
          team1: {
            owner: {
              email: 'owner@team1.com',
              uid: 'abc',
            },
            members: [
              'user1@team1.com',
              'user2@team1.com',
            ],
            documents: []
          },
          team2: {
            owner: {
              email: 'owner@team2.com',
              uid: 'def',
            },
            members: [
              'user1@team2.com',
            ],
            documents: []
          },
        }.dup
      end

      def public_key_path(owner)
        TEST_KEYS[owner][:public_key_path]
      end

      def private_key_path(owner)
        TEST_KEYS[owner][:private_key_path]
      end

      def import_keys
        TEST_KEYS.keys.each { |owner| import_key(owner) }
      end

      def import_key(owner, type = nil)
        TEST_KEYS[owner].each do |t, key|
          next if type && type != t
          GPGME::Key.import(key)
        end
      end

      def remove_keys
        TEST_KEYS.keys.each { |owner| remove_key(owner) }
      end

      def remove_all_keys
        GPGME::Key.find(:public).each { |k| k.delete!(true) }
        GPGME::Key.find(:secret).each { |k| k.delete!(true) }
      end

      def remove_key(owner)
        GPGME::Key.find(:public, owner).each { |k| k.delete!(true) }
        GPGME::Key.find(:secret, owner).each { |k| k.delete!(true) }
      end
    end
  end
end

GPGME::Engine.home_dir = Dir.tmpdir
Tmus::Test.remove_all_keys
Tmus::Test.import_keys


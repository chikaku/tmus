TEST_KEYS = {}

Dir.glob(File.expand_path(File.join('keys', '*'), File.dirname(__FILE__))).each do |f|
  email = f.split('/')[-1].sub(/\.pub$/, '')
  TEST_KEYS[email] ||= {}
  if f =~ /\.pub$/
    TEST_KEYS[email][:public] = IO.read(f)
    TEST_KEYS[email][:public_key_path] = f
  else
    TEST_KEYS[email][:private] = IO.read(f)
    TEST_KEYS[email][:private_key_path] = f
  end
end


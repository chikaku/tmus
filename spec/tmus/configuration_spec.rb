require 'spec_helper'
require 'securerandom'

RSpec.describe TeamMeUp::API do

  let(:config) { Tmus::Test.tmus_test_config }

  let(:a_team) { config.keys.sample }

  describe TeamMeUp::Configuration do
    context 'loading' do
      it 'can load a properly formatted configuration file' do
        sut = TeamMeUp::Configuration.new(config)
        expect(sut.send(:config)).to eq(config)
      end
    end

    context 'loaded' do
      before do
        allow(subject).to receive(:config).and_return(config)
      end

      context '#teams' do
        it 'returns the list of teams' do
          expect(subject.teams).to eq(config.keys.collect(&:to_s))
        end
      end

      context '#owner' do
        it 'returns "nil owner" when team is unknown' do
          team_name = SecureRandom.hex(3)
          expect {
            subject.owner(team_name)
          }.to raise_exception("#{team_name} does not exist")
        end

        it 'returns the team owner when team name as a string' do
          expect(subject.owner(a_team.to_s)).to eq(config[a_team][:owner])
        end

        it 'returns the team owner when team name as a symbol' do
          expect(subject.owner(a_team.to_sym)).to eq(config[a_team][:owner])
        end

        it 'returns the owner email address' do
          expect(subject.owner(a_team.to_sym)[:email]).to eq(config[a_team][:owner][:email])
        end

        it 'returns the owner GPG UID' do
          expect(subject.owner(a_team.to_sym)[:uid]).to eq(config[a_team][:owner][:uid])
        end
      end

      context '#members' do
        it 'raises an exception when team is unknown' do
          team_name = SecureRandom.hex(3)
          expect {
            subject.members(team_name)
          }.to raise_exception("#{team_name} does not exist")
        end

        it 'returns the team members when team name as a string' do
          expect(subject.members(a_team.to_s)).to eq(config[a_team][:members])
        end

        it 'returns the team members when team name as a symbol' do
          expect(subject.members(a_team.to_sym)).to eq(config[a_team][:members])
        end
      end

      context '#documents' do
        it 'raises an exception when team is unknown' do
          team_name = SecureRandom.hex(3)
          expect {
            subject.documents(team_name)
          }.to raise_exception("#{team_name} does not exist")
        end

        it 'returns an empty array for new teams' do
          team_name = SecureRandom.hex(3)
          subject.create(team_name, 'owner@me.com', 'uid')
          expect(subject.documents(team_name)).to eq []
        end

        it 'returns the team documents when team name as a string' do
          expect(subject.documents(a_team.to_s)).to eq(config[a_team][:documents])
        end

        it 'returns the team documents when team name as a symbol' do
          expect(subject.documents(a_team.to_sym)).to eq(config[a_team][:documents])
        end
      end

      context '#store' do
        it 'adds documents to a team' do
          path = SecureRandom.hex(3)
          expect(subject.store(path, a_team)).to include(path)
        end

        it 'does not add duplicate documents' do
          path = SecureRandom.hex(3)
          subject.store(path, a_team)
          expect {
            subject.store(path, a_team)
          }.to raise_exception("#{path} is already part of #{a_team}")
        end
      end

      context '#leave' do
        it 'removes documents from a team' do
          path = SecureRandom.hex(3)
          subject.store(path, a_team)
          expect(subject.dump(path, a_team)).not_to include(path)
        end

        it 'does remove documents that don\' exist' do
          path = SecureRandom.hex(3)
          expect {
            subject.dump(path, a_team)
          }.to raise_exception("#{path} is not part of #{a_team}")
        end
      end

      context '#create' do
        let(:email) { 'me@here.com' }
        let(:uid) { 'uid' }

        it 'add new teams' do
          expect(subject.create(SecureRandom.hex(3), email, uid)).to eq({
            owner: { email: email, uid: uid },
            members: [],
            documents: []
          })
        end

        it 'does not create duplicate teams' do
          team_name = SecureRandom.hex(3)
          subject.create(team_name, email, uid)
          expect {
            subject.create(team_name, email, uid)
          }.to raise_exception("#{team_name} already exists")
        end
      end

      context '#delete' do
        it 'removes a team' do
          team_name = SecureRandom.hex(3)
          subject.create(team_name, 'me@here.com', 'uid')
          expect(subject.delete(team_name)).to eq team_name
        end

        it 'does nothing if the team does not exist' do
          team_name = SecureRandom.hex(3)
          expect {
            subject.delete(team_name)
          }.to raise_exception("#{team_name} does not exist")
        end
      end

      context '#join' do
        it 'adds members to a team' do
          email = SecureRandom.hex(3) + '@test.com'
          expect(subject.join(email, a_team)).to include(email)
        end

        it 'does not add duplicate members' do
          email = SecureRandom.hex(3) + '@test.com'
          subject.join(email, a_team)
          expect {
            subject.join(email, a_team)
          }.to raise_exception("#{email} is already member of #{a_team}")
        end
      end

      context '#leave' do
        it 'removes members from a team' do
          email = SecureRandom.hex(3) + '@test.com'
          subject.join(email, a_team)
          expect(subject.leave(email, a_team)).not_to include(email)
        end

        it 'does remove members that don\' exist' do
          email = SecureRandom.hex(3) + '@test.com'
          expect {
            subject.leave(email, a_team)
          }.to raise_exception("#{email} is not a member of #{a_team}")
        end
      end
    end
  end
end


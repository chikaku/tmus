require 'spec_helper'
require 'securerandom'
require 'tempfile'

RSpec.describe TeamMeUp::API::Document do

  let(:config) { Tmus::Test.tmus_test_config }

  let(:a_team) { config.keys.sample }

  class SUT
    include TeamMeUp::API::Document

    def initialize(config)
      @configuration ||= TeamMeUp::Configuration.new(config)
      @crypto = GPGME::Crypto.new(password: '')
    end
  end

  subject { SUT.new(config) }

  context 'encrypt a document' do
    let(:clear_contents) { SecureRandom.hex(8) }

    it 'encrypts for all team members' do
      pending "I did not find yet how to bypass the local gpg-agent. Tested manually."
      contents_file = Tempfile.new
      begin
        contents_file.write(clear_contents)
        contents_file.close
        sut_encrypted = subject.encrypt(path: contents_file.path, team: a_team)
        expected_encryption = GPGME::Crypto.new.encrypt(clear_contents, recipients: Tmus::Test.tmus_test_config[a_team][:members])
        expect(sut_encrypted).to eq(expected_encryption)
      ensure
        contents_file.unlink
      end
    end
  end

  context '#store' do
 #   let(:content) { SecureRandom.hex(8) }

 #   # Setup the tests, and check sanity of the settings.
 #   before do
 #     @document = Tempfile.new
 #     @document.write(content)
 #     @document.close
 #     @secured_document_path = @document.path + '.enc'
 #     @result = subject.add(document: @document.path, team: a_team)
 #   end

 #   after do
 #     @document.unlink
 #   end

 #   it 'stores a new document in an existing team' do
 #     expect(subject.exists?(team: a_team, document: @document.path)).to be true
 #   end

 #   it 'stores the document locally with the .enc extension' do
 #     expect(File.exist?(@secured_document_path)).to be true
 #   end

 #   it 'erases the non-encrypted document locally' do
 #     expect(File.exist?(@document.path)).to be false
 #   end

 #   it 'encrypts the local document' do
 #     crypto = GPGME::Crypto.new
 #     data = GPGME::Data.from_str(IO.read(@secured_document_path))
 #     decoded = crypto.decrypt(data).to_s
 #     expect(decoded).to be eq content
 #   end

 #   it 'encrypts for all members of the team to read'

 #   it 'returns true on success' do
 #     expect(@result).to be true
 #   end
  end
end


require 'spec_helper'
require 'securerandom'

RSpec.describe TeamMeUp::API::Member do

  let(:config) { Tmus::Test.tmus_test_config }

  let(:a_team) { config.keys.sample }

  class SUT
    include TeamMeUp::API::Member

    def initialize(config)
      @configuration = TeamMeUp::Configuration.new(config)
    end

    def expel_from_test(email:, team:)
      @configuration.members(team).delete(email)
    end

    def test_in(email:, team:)
      @configuration.members(team).include?(email)
    end
  end

  subject { SUT.new(config) }

  it 'lists team members' do
    expect(subject.list(a_team)).to eq(config[a_team][:members])
  end

  it 'tests that team members exist' do
    expect(subject.exists?(team: a_team, email: config[a_team][:members].first)).to be true
  end

  it 'tests that team members do not exist' do
    expect(subject.exists?(team: a_team, email: config[a_team][:members].first)).to be true
  end

  context 'adding a member' do
    let(:email) { config[a_team][:members].sample }

    # Setup the tests, and check sanity of the settings.
    before do
      Tmus::Test.remove_key(email)
      subject.expel_from_test(email: email, team: a_team)
      expect(subject.exists?(team: a_team, email: email)).not_to be true
      expect(GPGME::Key.find(:public, email)).to be_empty
      expect(GPGME::Key.find(:private, email)).to be_empty
      expect(subject.list(a_team)).not_to include(email)
      @result = subject.add(email: email,
                            public_key_path: Tmus::Test.public_key_path(email),
                            team: a_team)
    end

    it 'adds a member to an existing team' do
      expect(subject.test_in(email: email, team: a_team)).to be true
    end

    it 'added member exists in the target team' do
      expect(subject.exists?(team: a_team, email: email)).to be true
    end

    it 'does not add a member twice' do
      expect(subject.add(email: email,
                         public_key_path: Tmus::Test.public_key_path(email),
                         team: a_team)).to be false
    end

    it 'does not add a member to an unknown team' do
      team_name = SecureRandom.hex(3)
      expect {
        subject.exists?(team: team_name, email: email)
      }.to raise_exception("#{team_name} does not exist")
    end

    it 'adds a member public key to the GPG dir' do
      expect(GPGME::Key.find(:public, email)).not_to be_empty
    end

    it 'does not add a private key to the GPG dir (only for owners)' do
      expect(GPGME::Key.find(:secret, email)).to be_empty
    end

    it 'adds the member to the team' do
      expect(subject.list(a_team)).to include(email)
    end

    it 'returns true on success' do
      expect(@result).to be true
    end
  end

  context 'removing a member' do
    let(:email) { config[a_team][:members].sample }

    before do
      subject.remove(email: email, team: a_team)
    end

    # Restore the test settings after runs.
    after do
      subject.add(email: email,
                  public_key_path: Tmus::Test.public_key_path(email),
                  team: a_team)
    end

    it 'removes a member from its team' do
      expect(subject.test_in(email: email, team: a_team)).to be false
    end

    it 'removes a member that existed in the target team' do
      expect(subject.exists?(email: email, team: a_team)).to be false
    end

    it 'removes the member public key from the GPG dir' do
      expect(GPGME::Key.find(:public, email)).to be_empty
    end

    it 'does not modify the private key list' do
      expect(GPGME::Key.find(:secret, email)).to be_empty
    end
  end
end


Team Me Up, Scotty --- All for One, and One for All
===================================================

TMUS ("team-us") is a mini-toolchain that manages documents with GPG security.

The original intent is to be able for a team to share documents
securely, still leveraging version control. Typical use is to
commit to version control some mildly sensitive configuration files.

Security is based on GPG and GPGME. All secret files are encrypted
by a team leader, for *all* team members to be able to read. This
mechanism uses a public key per team member to encrypt the
documents---a classical use case for GPG.

Warning
-------

TMUS is work in progress, and in its early stage. Use with care, and
do not rely on this tool to protect really sensitive information.
The mechanism is secure if used properly, and the API tries to make
it simple to use. But there may be bugs, and some unforseen use cases
may bypass important stages. In doubt, you can double check the
level of security with the raw GPGME or GPG tools.

Get Started
-----------

Just issue the base command to get basic explanation:

    ./bin/tmus

If you are using `tmus` as a gem, documentation is lacking at this point.
The starting point is to require the gem and use the `TeamMeUp::API::Document`
module.

    require 'tmus'
    clear_contents = TeamMeUp::IO.read('.env')

This code snippet will make the contents of the encrypted `.env` file (`tmus`
names it `.env.tmus`) available to your program runtime.

Usage
-----

### Typical workflow

Let's assume you want to take the lead on the `my_team` team, with
your `owner@team.com` address.

* Prepare a GPG key pair for the team owner. Usually, `gpg --gen-key`, then you can get your detail with `gpg -k owner@team.com`.
* Install `tmus` (e.g. add it to your Gemfile). The gem provides the `tmus` command (`bundle exec tmus`), and an API to use in code.
* Initialize a team: `tmus init my_team owner@team.com my_uid`
* Add members to the team: `tmus members add rebecca@team.com ~/tmp/rebecca.pub my_team`
* Add documents to the team: `tmus documents add .env my_team`
* Add `.tmus/config` and the encrypted files to version control.
* Now all team members can leverage the encrypted files locally.

You can ignore several files from version control: `.tmus/backups` and all the
non-encrypted versions of files added with `tmus` should be ignored (e.g.
added to `.gitignore`).

### In code

We are in the era of automation. Let's assume that you want to automate
deployment or build of your code, but the code requires the clear contents of
the files managed by `tmus`. You can use the API to access that contents---and
only team members will be able to do it. Let's look at some production code
that we is actually used somewhere!

    # config/deploy.rb for Capistrano 3

    require 'tmus'
    local_dotenv_path = File.expand_path("../.env.#{fetch :stage}", File.dirname(__FILE__))
    team_dotenv = TeamMeUp::IO.read(".env.#{fetch :stage}")
    if File.exist? local_dotenv_path
      local_dotenv = File.read(local_dotenv_path)
      if local_dotenv != team_dotenv
        puts "Your dotenv in #{local_dotenv_path} differs from the team settings."
        puts 'Please backup and remove your local file and run again.'
        puts 'This script will create the team file, and you can check its contents.'
        exit 1
      end
    else
      puts "#{local_dotenv_path} not found. Generating from the team settings..."
      File.open(local_dotenv_path, 'w') { |f| f.write team_dotenv }
      puts "Generated #{local_dotenv_path}"
    end
    
    require 'dotenv'
    Dotenv.load(
      File.expand_path("../.env.#{fetch :stage}", File.dirname(__FILE__)),
      File.expand_path('../.env', File.dirname(__FILE__))
    )
    
    lock '3.4.0'
    
    set :application, 'My Site'
    set :repo_url, 'git@github.com:my_company/my_site'
    
    ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
    
    set :deploy_to, '/var/www/my_site'
    
    set :scm, :git

    # etc.

The key part is:

    require 'tmus'
    # ...
    team_dotenv = TeamMeUp::IO.read(".env.#{fetch :stage}")

This code assumes the file named `.env.#{fetch :stage}`
(e.g. `.env.production`) is managed by `tmus`. One way to make sure of it is
that the repository should contain a `.env.production.tmus` file. Then the code
can access the clear contents of the file, for the duration of the runtime.

Roadmap
-------

* Modify documents when members change
* Adopt a stage/commit/push model for the configuration DS (what I have in mind
  is related but slightly different from Git---sorry for the terminology clash
  for now).
* Find a way to test encryption/decryption units automatically.


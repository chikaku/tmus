require 'yaml'
require 'fileutils'
require 'securerandom'

module TeamMeUp
  class Configuration
    DOT_TMUS_DIR = File.join(Dir.pwd, '.tmus')
    CONFIG_FILE = File.join(DOT_TMUS_DIR, 'config')
    BACKUP_DIR = File.join(DOT_TMUS_DIR, 'backups')

    def initialize(override = nil)
      @config = override
      FileUtils.mkdir_p(BACKUP_DIR)
    end

    def teams
      config.keys.collect(&:to_s)
    end

    def has_team?(name)
      config.keys.include?(name.to_sym)
    end

    def owner(team)
      raise "#{team} does not exist" unless has_team?(team)
      config[team.to_sym][:owner]
    end

    def members(team)
      raise "#{team} does not exist" unless has_team?(team)
      # TODO config[team.to_sym][:members].dup
      config[team.to_sym][:members]
    end

    def documents(team)
      raise "#{team} does not exist" unless has_team?(team)
      config[team.to_sym][:documents].dup
    end

    def create(team, email, uid)
      raise "#{team} already exists" if has_team?(team)
      config[team.to_sym] = { owner: { email: email, uid: uid }, members: [], documents: [] }
      config[team.to_sym]
    end

    def delete(team)
      raise "#{team} does not exist" unless has_team?(team)
      config.delete(team.to_sym)
      team
    end

    def join(email, team)
      raise "#{team} does not exist" unless has_team?(team)
      raise "#{email} is already member of #{team}" if members(team).include?(email)
      config[team.to_sym][:members] << email
      members(team)
    end

    def leave(email, team)
      raise "#{team} does not exist" unless has_team?(team)
      raise "#{email} is not a member of #{team}" unless members(team).include?(email)
      config[team.to_sym][:members].delete(email)
      members(team)
    end

    def store(document, team)
      raise "#{team} does not exist" unless has_team?(team)
      raise "#{document} is already part of #{team}" if documents(team).include?(document)
      config[team.to_sym][:documents] << document
      documents(team)
    end

    def dump(document, team)
      raise "#{team} does not exist" unless has_team?(team)
      raise "#{document} is not part of #{team}" unless documents(team).include?(document)
      config[team.to_sym][:documents].delete(document)
      documents(team)
    end

    def save
      backup
      serialize
      true
    end

    private

    def config
      @config ||= if File.exist?(CONFIG_FILE)
                    YAML.load(decrypt(File.read(CONFIG_FILE)))
                  else
                    {}
                  end
    end

    def crypto
      @crypto ||= GPGME::Crypto.new
    end

    # Note: `always_trust` is set! We assume that all team owners and the members are trusted.
    def encrypt(contents)
      recipients = teams.collect { |t| owner(t)[:email] }
      recipients << teams.inject([]) do |arr, team|
        arr << members(team) unless members(team).empty?
        arr
      end
      crypto.encrypt(contents,
                     recipients: recipients,
                     always_trust: true).to_s
    end

    def decrypt(contents)
     crypto.decrypt(GPGME::Data.from_str(contents)).to_s
    end

    def serialize
      File.open(CONFIG_FILE, 'w') { |f| f.write encrypt(YAML.dump(config)) }
    end

    def backup
      FileUtils.copy_file(
        CONFIG_FILE,
        File.join(BACKUP_DIR, "#{File.basename(CONFIG_FILE)}-#{Time.now.strftime("%Y%m%d%H%M%S")}-#{SecureRandom.hex(3)}")
      ) if File.exist?(CONFIG_FILE)
    end
  end
end


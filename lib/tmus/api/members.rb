require 'gpgme'
require 'yaml'

module TeamMeUp
  module API
    module Member
      def list(team)
        @users ||= config.members(team)
      end

      def add(email:, public_key_path:, team:)
        return false if exists?(email: email, team: team)
        return false unless config.has_team?(team)
        GPGME::Key.import(File.open(public_key_path))
        config.join(email, team)
        true
      end

      def exists?(email:, team:)
        (config.members(team) || []).include?(email) &&
          (
            !GPGME::Key.find(:secret, email).empty? ||
            !GPGME::Key.find(:public, email).empty?
          )
      end

      def remove(email:, team:)
        return false unless config.has_team?(team)
        GPGME::Key.find(:public, email).each { |k| k.delete!(true) }
        if config.members(team).include?(email)
          config.leave(email, team)
          true
        else
          false
        end
      end

      private

      # Ensure the default configuration is available to the module.
      def config
        @configuration ||= TeamMeUp::Configuration.new
      end
    end

    module Document
      def list(team)
        @documents ||= config.documents(team)
      end

      def add(document:, team:)
        return false if exists?(document: document, team: team)
        return false unless config.has_team?(team)
        config.store(document, team)
        true
      end

      def exists?(document:, team:)
        (config.documents(team) || []).include?(document)
      end

      def remove(document:, team:)
        return false unless config.has_team?(team)
        if config.documents(team).include?(email)
          config.dump(document, team)
          true
        else
          false
        end
      end

      def encrypt(path:, team:)
        # Note: `always_trust` is set! We assume that all team members are trusted.
        crypto.encrypt(IO.read(path),
                       recipients: @configuration.members(team),
                       always_trust: true).to_s
      end

      def decrypt(path)
        data = GPGME::Data.from_str(IO.read(path))
        crypto.decrypt(data).to_s
      end

      def crypto
        @crypto ||= GPGME::Crypto.new
      end

      # Ensure the default configuration is available to the module.
      def config
        @configuration ||= TeamMeUp::Configuration.new
      end
    end
  end
end


require 'gpgme'
require 'fileutils'

module TeamMeUp
  module API
    module Document
      EXTENSION = '.tmus'
      BACKUP_EXTENSION = '.original.tmus.do_not_commit_to_version_control'

      def list(team)
        @documents ||= config.documents(team)
      end

      def add(document:, team:)
        return false if exists?(document: document, team: team)
        return false unless config.has_team?(team)
        return false if config.members(team).empty?
        absolute_path = File.expand_path(document)
        encoded = encrypt(path: absolute_path, team: team)
        File.open(absolute_path + EXTENSION, 'w') { |f| f.write encoded }
        config.store(document, team)
        FileUtils.mv(absolute_path, absolute_path + BACKUP_EXTENSION)
        true
      end

      def exists?(document:, team:)
        (config.documents(team) || []).include?(document)
      end

      def remove(document:, team:)
        return false unless config.has_team?(team)
        return false unless config.documents(team).include?(document)
        if config.documents(team).include?(document)
          absolute_path = File.expand_path(document)
          decoded = decrypt(absolute_path + EXTENSION)
          File.open(absolute_path, 'w') { |f| f.write decoded }
          config.dump(document, team)
          FileUtils.rm_f(absolute_path + EXTENSION)
          FileUtils.rm_f(absolute_path + BACKUP_EXTENSION)
          true
        else
          false
        end
      end

      def get(document:, team:)
        return false unless config.has_team?(team)
        return false unless config.documents(team).include?(document)
        unless config.documents(team).include?(document)
          return false
        end
        absolute_path = File.expand_path(document)
        return decrypt(absolute_path + EXTENSION)
      end

      private

      # Notes:
      #   - `always_trust` is set! We assume that team members are all trusted.
      #   - The owner is added to the recipients list, so that she can
      #     decrypt the document too.
      def encrypt(path:, team:)
        crypto.encrypt(IO.read(path), 
          recipients: @configuration.members(team) + [@configuration.owner(team)[:email]],
          always_trust: true
        ).to_s
      end

      def decrypt(path)
        crypto.decrypt(File.open(path)).to_s
      end

      def crypto
        @crypto ||= GPGME::Crypto.new
      end

      # Ensure the default configuration is available to the module.
      def config
        @configuration ||= TeamMeUp::Configuration.new
      end
    end
  end
end


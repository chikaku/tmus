require 'thor'
require 'yaml'
require 'gpgme'

module TeamMeUp
  module CLI
    TEAM_ALREADY_EXISTS = 1
    TEAM_NOT_FOUND = 2
    MEMBER_NOT_ADDED = 3
    MEMBER_NOT_REMOVED = 4
    DOCUMENT_NOT_ADDED = 5
    DOCUMENT_NOT_REMOVED = 6
    DOCUMENT_NOT_FOUND = 7

    class Teams < Thor
      desc 'list', 'List all teams.'
      def list_teams()
        teams = config.teams
        if teams.empty?
          puts 'No team'
        else
          teams.each { |m| puts m }
        end
      end

      no_commands do
        def config
          @configuration ||= TeamMeUp::Configuration.new
        end
      end
    end

    class Members < Thor
      include TeamMeUp::API::Member

      desc 'list TEAM', 'List the team members.'
      def list_members(team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        members = config.members(team)
        if members.empty?
          puts 'No member'
        else
          members.each { |m| puts m }
        end
      end

      desc 'add EMAIL KEY_PATH TEAM', 'Add a user to the team'
      def add_member(email, key_path, team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        if add(email: email, public_key_path: key_path, team: team)
          config.save
        else
          puts "Failed to add #{email} to #{team}"
          exit MEMBER_NOT_ADDED
        end
      end

      desc 'remove EMAIL TEAM', 'Remove a user from a team'
      def remove_member(email, team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        if remove(email: email, team: team)
          config.save
        else
          puts "Failed to remove #{email} from #{team}. Do both exist?"
          exit MEMBER_NOT_REMOVED
        end
      end

      no_commands do
        def config
          @configuration ||= TeamMeUp::Configuration.new
        end
      end
    end

    class Documents < Thor
      include TeamMeUp::API::Document

      desc 'list TEAM', 'List the team documents.'
      def list_documents(team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        documents = config.documents(team)
        if documents.empty?
          puts 'No document'
        else
          documents.each { |m| puts m }
        end
      end

      desc 'add DOCUMENT TEAM', 'Add a document to the team'
      def add_document(document, team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        if add(document: document, team: team)
          config.save
        else
          puts "Failed to add #{document} to #{team}"
          exit DOCUMENT_NOT_ADDED
        end
      end

      desc 'remove DOCUMENT TEAM', 'Remove a document from a team'
      def remove_document(document, team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        if remove(document: document, team: team)
          config.save
        else
          puts "Failed to remove #{document} from #{team}. Do both exist?"
          exit DOCUMENT_NOT_REMOVED
        end
      end

      desc 'show DOCUMENT TEAM', 'Show a document of the team'
      def show_document(document, team)
        unless config.has_team?(team)
          puts "Team \"#{team}\" not found."
          exit TEAM_NOT_FOUND
        end
        unless exists?(document: document, team: team)
          puts "Document \"#{document}\" not found."
          exit DOCUMENT_NOT_FOUND
        end
        content = get(document: document, team: team)
        if content == false
          puts "Failed to get #{document} from #{team}."
          exit DOCUMENT_NOT_FOUND
        end
        puts content
      end

      no_commands do
        def config
          @configuration ||= TeamMeUp::Configuration.new
        end
      end
    end

    class Tmus < Thor
      desc "init NAME EMAIL UID", 'Initialize a team managed by the key UID and email\'s owner'
      long_desc <<-LONGDESC
        Initialize a new team settings. This command will create a `TMUS`
        file with the team details. If `.tmus.yml` already exists, a new team
        will be appended to it.

        NAME is the name of the team.

        EMAIL is the email of the team leader. Only the team leader can encrypt
              data for the team.

        UID   GPG UID of the key used for encryption. Get your key list with `gpg -K`,
              and choose either a UID (user name / email pair), or its unique number.
              If no key is available, you can generate one with `gpg --gen-key`.

        For example, running `gpg -K` displays:

        > ✗ gpg -K
        \x5> /home/dl/.gnupg/secring.gpg
        \x5> ------------------------------------
        \x5> sec   4096R/D0B4BCCA 2013-11-25 [expires: 2017-11-25]
        \x5> uid                  Didier Lembrouille <didier@lembrouille.io>
        \x5> ssb   4096R/173B3D18 2013-11-25
        \x5>
        \x5> sec   4096R/45FA3695 2016-04-01
        \x5> uid                  Didier Lembrouille <didier@lembrouille.com>
        \x5> ssb   4096R/D8952D05 2016-04-01

        Valid UIDs are either '45FA3695' or 'Didier Lembrouille <didier@lembrouille.com>'
        for the second key.
      LONGDESC
      def init(name, email, uid)
        if config.teams.include?(name)
          puts "The #{name} team already exists, owned by #{config.owner(name)}. Aborting init."
          exit TEAM_ALREADY_EXISTS
        end

        created = config.create(name, email, uid)
        config.save

        puts "Added team: #{created}"
        created
      end

      desc 'teams SUBCOMMAND', 'Commands to manage teams.'
      subcommand 'teams', Teams

      desc 'members SUBCOMMAND', 'Commands to manage members.'
      subcommand 'members', Members

      desc 'documents SUBCOMMAND', 'Commands to manage documents.'
      subcommand 'documents', Documents

      no_commands do
        def config
          @config ||= TeamMeUp::Configuration.new
        end
      end
    end
  end
end


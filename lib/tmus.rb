require 'gpgme'

require 'tmus/api/documents'

module TeamMeUp
  class IO
    class << self
      def read(path)
        path += TeamMeUp::API::Document::EXTENSION unless path =~ /#{TeamMeUp::API::Document::EXTENSION}$/
        crypto.decrypt(File.open(path)).to_s
      end

      private

      def crypto
        @@crypto ||= GPGME::Crypto.new
      end
    end
  end
end

